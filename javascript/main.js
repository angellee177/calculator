console.error("this is use console error!"); // for Error.
console.warn("this is use console warning!");// for warning.
alert('you are learning Javascript!'); // to get pop up on Browser.

// const
const age = 23;
console.log(age);

/**
 * let, you need to define the "let" variable first.
 * you can't do something like ex: "let score = 10, console.log(score);"
 * */
let score;
score = 10;
console.log(score);

/**
 * There is a few Data Types on Javascript,
 * like String, Numbers, Boolean, null, undefined
 */
const name = 'Angel';
console.log(name+' '+typeof name);

const number = 23;
console.log(number+' '+typeof number);

const rating = 4.5;
console.log(rating+' '+ typeof rating);

const isCool = true;
console.log(isCool+' '+typeof isCool);

const x = null;
console.log(x+' '+ typeof x);

const y = undefined;
console.log(y+' '+typeof y);

let z;
z = undefined;
console.log(z+' '+typeof z);