// // Function untuk penambahan
function plus(a, b, c, d){
    console.log(a + b + c + d );
};

plus("ini", 7, 9, 10);

// if Else statement
var a = 0
if ( typeof a === 'number'){
    console.log("your lucky number is "+ a);
} else {
    console.log("inputan harus angka!");
}

// Function tambah dengan condition
function hello(x, y){
    if( typeof x !== 'number' || typeof y !== 'number') {
        return console.log("Inputan harus Angka!");
    };
    console.log(x + y);
}
hello(4, 5);


// Function kurang dengan condition
function kurang(x, y, z){
    if(typeof x !== 'number' || typeof y !== 'number' || typeof z !== 'number')
    {
        return console.log ("Inputan harus angka!"); 
        /** kalau kita tidak menambahkan return maka function akan terus berjalan sampai console.log kedua.
        *   sebaliknya jika kita menambahkan "return", maka function akan langsung berhenti di console.log pertama,
        *   jika statement pertama salah.
        */
    };
    console.log(x-y-z);
}

kurang(1,2,"string");

// Function bagi dengan condition.
function bagi(x, y, z){
    if(typeof x !== 'number' || typeof y !== 'number' || typeof z !== 'number')
    {
        return console.log ("Inputan harus angka!"); 
        /** kalau kita tidak menambahkan return maka function akan terus berjalan sampai console.log kedua.
        *   sebaliknya jika kita menambahkan "return", maka function akan langsung berhenti di console.log pertama,
        *   jika statement pertama salah.
        */
    };
    console.log(x/y/z);
}

bagi(8,1,2);

// Function Kali dengan condition.
function kali(x, y, z){
    if(typeof x !== 'number' || typeof y !== 'number' || typeof z !== 'number')
    {
        return console.log ("Inputan harus angka!"); 
        /** kalau kita tidak menambahkan return maka function akan terus berjalan sampai console.log kedua.
        *   sebaliknya jika kita menambahkan "return", maka function akan langsung berhenti di console.log pertama,
        *   jika statement pertama salah.
        */
    };
    console.log(x*y*z);
}

kali(1,2,3);

// Function Aritmatika
function kurang(a, b, c, d, e){
    if(typeof a !== 'number' || typeof b !== 'number' 
    || typeof c !== 'number' || typeof d !== 'number'
    || typeof e !== 'number')
    {
        return console.log ("Inputan harus angka!"); 
        /** kalau kita tidak menambahkan return maka function akan terus berjalan sampai console.log kedua.
        *   sebaliknya jika kita menambahkan "return", maka function akan langsung berhenti di console.log pertama,
        *   jika statement pertama salah.
        */
    };
    console.log(a+b-(c*d/e));
}

kurang(8,2,4,5,2);

// Function calculator with If Else Statement and Switch Case.
function calculator(value1, value2, method){
    if(typeof value2 !== 'number')
    {
        return console.log("Inputan harus angka!"); 
        /** kalau kita tidak menambahkan return maka function akan terus berjalan sampai console.log kedua.
        *   sebaliknya jika kita menambahkan "return", maka function akan langsung berhenti di console.log pertama,
        *   jika statement pertama salah.
        */
    }else if (typeof value1 !== 'number'){
        return console.log("inputan harus angka!");
    }
    switch(method){
    case "*":
        console.log(value1*value2); 
    break;
    case "/" :
        console.log(value1/value2);
    break;
    case "-":
        console.log(value1-value2);
    break;
    case "+":
        console.log(value1+value2);
    break;

    default:
        console.log("operator salah!!");
    break;
    }

}
calculator(9, 10, "+");